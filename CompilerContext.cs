﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace sh2ezo.utils.ExpressionCompiler
{
    internal class CompilerContext
    {
        public Stack<Token> Tokens { get; private set; }
        public Dictionary<string, int> Arguments { get; private set; }
        public ParameterExpression InputsExpression { get; private set; }

        public CompilerContext(Stack<Token> tokens, Dictionary<string, int> arguments, ParameterExpression inputsExpression)
        {
            Tokens = tokens;
            Arguments = arguments;
            InputsExpression = inputsExpression;
        }
    }
}
