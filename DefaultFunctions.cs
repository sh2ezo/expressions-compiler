﻿using sh2ezo.utils.ExpressionCompiler.Exceptions;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace sh2ezo.utils.ExpressionCompiler
{
    public static class DefaultFunctions
    {
        public static IReadOnlyDictionary<string, MethodInfo> Functions { get; private set; } = GetFunctions();

        private static Dictionary<string, MethodInfo> GetFunctions()
        {
            var methods = typeof(Math).GetMethods();
            var result = new Dictionary<string, MethodInfo>();

            foreach(var method in methods)
            {
                var parameters = method.GetParameters();

                if (parameters.Length != 1)
                {
                    continue;
                }

                if(parameters[0].ParameterType != typeof(double))
                {
                    continue;
                }

                if(method.DeclaringType != typeof(Math))
                {
                    continue;
                }

                result.Add(method.Name.ToLower().ToString(), method);
            }

            return result;
        }

        public static MethodInfo GetFunction(string identifier)
        {
            identifier = identifier.ToLower();

            if(Functions.TryGetValue(identifier, out var method))
            {
                return method;
            }

            throw new UnknownFunctionException(identifier);
        }
    }
}
