﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sh2ezo.utils.ExpressionCompiler.Exceptions
{
    public class UnknownFunctionException : Exception
    {
        public UnknownFunctionException(string identifier) : base("unknown function: " + identifier)
        {

        }
    }
}
