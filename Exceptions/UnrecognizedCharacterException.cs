﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sh2ezo.utils.ExpressionCompiler.Exceptions
{
    public class UnrecognizedCharacterException : Exception
    {
        public UnrecognizedCharacterException(char c) : base("unrecognized character: " + c)
        {

        }
    }
}
