﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sh2ezo.utils.ExpressionCompiler.Exceptions
{
    public class WrongTokenException : Exception
    {
        internal WrongTokenException(Token token) : base("wrong token: " + token.Value)
        {

        }
    }
}
