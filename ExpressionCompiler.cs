﻿using sh2ezo.utils.ExpressionCompiler.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using sh2ezo.utils.ExpressionsCompiler;

namespace sh2ezo.utils.ExpressionCompiler
{
    public static class ExpressionCompiler
    {
        private readonly static MethodInfo _powMethod = typeof(Math).GetMethod("Pow", new Type[] { typeof(double), typeof(double) });

        public static Function Compile(string expression)
        {
            var context = new CompilerContext(
                tokens: Tokenizer.GetTokens(expression),
                arguments: new Dictionary<string, int>(),
                inputsExpression: Expression.Parameter(typeof(double[]), "inputs")
            );

            var builtExpression = BuildExpression(context, false);

            while (builtExpression.CanReduce)
            {
                builtExpression = builtExpression.Reduce();
            }

            return new Function
            {
                Arguments = context.Arguments,
                Delegate = Expression.Lambda<Func<double[], double>>(
                    body: builtExpression,
                    parameters: new ParameterExpression[] {
                        context.InputsExpression
                    }
                ).Compile(false)
            };
        }

        private static Expression BuildExpression(CompilerContext context, bool brackets)
        {
            Expression expression = null;

            while(context.Tokens.TryPeek(out var token))
            {
                if (token.Type == TokenType.OpenBracket)
                {
                    if (brackets)
                    {
                        break;
                    }
                    else
                    {
                        throw new InvalidOperationException("not closed bracket");
                    }
                }
                else if (TryGetOperandExpression(context, out var operand))
                {
                    if (expression != null)
                    {
                        throw new InvalidOperationException("two operands without operator");
                    }

                    expression = operand;
                }
                else if (token.Type == TokenType.Operator)
                {
                    expression = BuildOperation(
                        @operator: token.Value,
                        rightOperand: expression,
                        context: context,
                        brackets: brackets
                    );
                }
                else
                {
                    throw new InvalidOperationException("unexpected token: " + token.Value);
                }
            }

            return expression;
        }

        private static bool TryGetOperandExpression(CompilerContext context, out Expression expression)
        {
            if(!context.Tokens.TryPeek(out var token))
            {
                expression = null;
                return false;
            }

            if (token.Type == TokenType.CloseBracket)
            {
                context.Tokens.Pop();
                expression = BuildExpression(context, true);

                if (context.Tokens.Pop().Type != TokenType.OpenBracket)
                {
                    throw new WrongTokenException(token);
                }

                if (context.Tokens.TryPeek(out var nextToken) && nextToken.Type == TokenType.Identifier)
                {
                    expression = Expression.Call(
                        null,
                        DefaultFunctions.GetFunction(nextToken.Value),
                        expression
                    );

                    context.Tokens.Pop();
                }
            }
            else if (token.Type == TokenType.Number)
            {
                expression = Expression.Constant(double.Parse(token.Value));

                context.Tokens.Pop();
            }
            else if (token.Type == TokenType.Identifier)
            {
                context.Arguments.Add(token.Value, context.Arguments.Count);

                expression = Expression.ArrayAccess(
                    array: context.InputsExpression,
                    indexes: Expression.Constant(context.Arguments.Count - 1)
                );

                context.Tokens.Pop();
            }
            else
            {
                expression = null;
                return false;
            }

            return true;
        }

        private static Expression BuildOperation(string @operator, Expression rightOperand, CompilerContext context, bool brackets)
        {
            if (@operator == "*")
            {
                context.Tokens.Pop();

                if (!TryGetOperandExpression(context, out var leftOperand))
                {
                    throw new InvalidOperationException("no operand for *");
                }

                return Expression.Multiply(leftOperand, rightOperand);
            }
            else if(@operator == "/")
            {
                context.Tokens.Pop();

                if (!TryGetOperandExpression(context, out var leftOperand))
                {
                    throw new InvalidOperationException("no operand for /");
                }

                return Expression.Divide(leftOperand, rightOperand);
            }
            else if (@operator == "^")
            {
                context.Tokens.Pop();

                if (!TryGetOperandExpression(context, out var leftOperand))
                {
                    throw new InvalidOperationException("no operand for /");
                }

                return Expression.Call(null, _powMethod, new Expression[] { leftOperand, rightOperand });
            }
            else if (@operator == "+")
            {
                context.Tokens.Pop();

                if(!context.Tokens.TryPeek(out var nextToken))
                {
                    return rightOperand;
                }
                else if(nextToken.Type == TokenType.OpenBracket || nextToken.Type == TokenType.Operator)
                {
                    return rightOperand;
                }

                var leftOperand = BuildExpression(context, brackets);

                return Expression.Add(leftOperand, rightOperand);
            }
            else if (@operator == "-")
            {
                context.Tokens.Pop();

                if (!context.Tokens.TryPeek(out var nextToken))
                {
                    return Expression.Negate(rightOperand);
                }
                else if (nextToken.Type == TokenType.OpenBracket || nextToken.Type == TokenType.Operator)
                {
                    return Expression.Negate(rightOperand);
                }

                var leftOperand = BuildExpression(context, brackets);

                return Expression.Subtract(leftOperand, rightOperand);
            }
            else
            {
                throw new InvalidOperationException("unknown operator: " + @operator);
            }
        }
    }
}
