﻿using System;
using System.Collections.Generic;

namespace sh2ezo.utils.ExpressionCompiler
{
    public class Function
    {
        public Func<double[], double> Delegate { get; set; }
        public Dictionary<string, int> Arguments { get; set; }
    }
}