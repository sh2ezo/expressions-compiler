﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sh2ezo.utils.ExpressionsCompiler
{
    internal static class StackExtensions
    {
        public static bool TryPeek<T>(this Stack<T> stack, out T value)
        {
            if (stack.Count == 0)
            {
                value = default(T);
                return false;
            }

            value = stack.Pop();
            return true;
        }
    }
}
