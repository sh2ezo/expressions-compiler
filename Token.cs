﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sh2ezo.utils.ExpressionCompiler
{
    internal class Token
    {
        public string Value { get; set; }
        public TokenType Type { get; set; }

        public Token(string value, TokenType type)
        {
            Value = value;
            Type = type;
        }
    }
}
