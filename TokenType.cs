﻿namespace sh2ezo.utils.ExpressionCompiler
{
    internal enum TokenType
    {
        Number,
        OpenBracket,
        CloseBracket,
        Identifier,
        Operator
    }
}