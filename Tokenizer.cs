﻿using sh2ezo.utils.ExpressionCompiler.Exceptions;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;

namespace sh2ezo.utils.ExpressionCompiler
{
    internal class Tokenizer
    {
        public static Stack<Token> GetTokens(string source)
        {
            var result = new Stack<Token>();

            using (var reader = new StringReader(source))
            {
                for (int peekedValue = reader.Peek(); peekedValue != -1; peekedValue = reader.Peek())
                {
                    char peekedChar = (char)peekedValue;

                    if (peekedChar == '(')
                    {
                        result.Push(new Token(peekedChar.ToString(), TokenType.OpenBracket));
                        reader.Read();
                    }
                    else if(peekedChar == ')')
                    {
                        result.Push(new Token(peekedChar.ToString(), TokenType.CloseBracket));
                        reader.Read();
                    }
                    else if (char.IsDigit(peekedChar))
                    {
                        var number = ReadNumber(reader);

                        result.Push(new Token(number, TokenType.Number));
                    }
                    else if (char.IsLetter(peekedChar))
                    {
                        var identifier = ReadWord(reader);

                        result.Push(new Token(identifier, TokenType.Identifier));
                    }
                    else if (IsOperator(peekedChar))
                    {
                        result.Push(new Token(peekedChar.ToString(), TokenType.Operator));
                        reader.Read();
                    }
                    else if (peekedChar == ' ')
                    {
                        reader.Read();
                    }
                    else
                    {
                        throw new UnrecognizedCharacterException(peekedChar);
                    }
                }
            }

            return result;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static bool IsOperator(char c)
        {
            return c == '+' || c == '-' || c == '*' || c == '/' || c == '^';
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static bool IsBracket(char c)
        {
            return c == '(' || c == ')';
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static bool IsSeparator(char c)
        {
            return IsBracket(c) || IsOperator(c) || c == ' ';
        }

        private static string ReadNumber(StringReader reader)
        {
            StringBuilder builder = new StringBuilder();

            for(int peekedValue = reader.Peek(); peekedValue != -1; peekedValue = reader.Peek())
            {
                char peekedChar = (char)peekedValue;

                if (char.IsDigit(peekedChar) || peekedChar == '.')
                {
                    builder.Append(peekedChar);
                }
                else if (IsSeparator(peekedChar))
                {
                    break;
                }
                else
                {
                    throw new UnrecognizedCharacterException(peekedChar);
                }

                reader.Read();
            }

            return builder.ToString();
        }

        private static string ReadWord(StringReader reader)
        {
            StringBuilder builder = new StringBuilder();

            for (int peekedValue = reader.Peek(); peekedValue != -1; peekedValue = reader.Peek())
            {
                char peekedChar = (char)peekedValue;

                if (char.IsLetterOrDigit(peekedChar) || peekedChar == '_')
                {
                    builder.Append(peekedChar);
                }
                else if (IsSeparator(peekedChar))
                {
                    break;
                }
                else
                {
                    throw new UnrecognizedCharacterException(peekedChar);
                }

                reader.Read();
            }

            return builder.ToString();
        }
    }
}
